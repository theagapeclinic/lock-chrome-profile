rem `icacls` documentation available at https://ss64.com/nt/icacls.html

rem user profile "Default"
icacls^
 "%LOCALAPPDATA%\Google\Chrome\User Data\Default\Extensions"^
 /deny^
 "%USERNAME%":(OI)(CI)W

rem user profile "Profile 1"
icacls^
 "%LOCALAPPDATA%\Google\Chrome\User Data\Profile 1\Extensions"^
 /deny^
 "%USERNAME%":(OI)(CI)W
